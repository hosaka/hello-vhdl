entity simp is
  port (
    i1, i2 : in bit;
    o : out bit
  ) ;
end entity ; -- simp

architecture arch of simp is

    signal int : bit;

begin
    int <= i1 AND i2; -- available to the outside world
    o <= NOT int;
end architecture ; -- arch