-------------------------------------------------------------------------------
-- VHDL Basics Altera Course: The code is not to be compiled, for study only --
-------------------------------------------------------------------------------
--
-- TODO: Describe Bahavior and Structural Modeling / Register Transfer Level
--
-----------------------
-- VHDL Design Units --
-----------------------
--
-- Entity: used to define external view of a model (analogy:  symbol)
entity hello is

  -- Generic declaration used to pass information to an entity at compile time
  -- Allowing for parameters and variables
  generic (
    -- <class> object_name: <type> := <value>;
    -- type is a data type
    constant tplh, tplh : time := 5ns; -- constant is assumed and not required
    tphz, tplz : time := 3ns;
    default_value : integer := 1;
    cnt_dir : string := "up" -- note the absence of ; since it's a list
  );

  -- Port declaration specifies the input/output to the entity
  port (
    -- <class> object_name: <mode> <type>;
    -- mode is the direction of the port: in, out, inout(bidirect), buffer
    -- type is what can be contained in the object
    signal clock, clear : in bit; -- signal is assumed and not required
    q : out bit
  ) ;
end entity ; -- hello

-- Architecture: used to define the function of a model (analogy: schematic with symbols)
-- Describes the internal logic, you can see the entity name, params, the
-- outputs, but no visibility of the entity guts
-- Must be associated with an entity, but entity can belong to multiple arch
architecture arch of hello is
    -- arch declaration section
    signal temp : integer := 1; -- signal declaration is default (optional)
    constant load : boolean := true;
    type states is (S1, S2, S3, S4); -- type declarations
    -- Component decl
    -- Subtype decl
    -- Attribute decl
    -- Attribute spec
    -- Subprogram decl
    -- Subprogram body
begin
    -- Architecture body: contains executable lines executed concurrently

end architecture ; -- arch

-- Configuration: used to associate an entity with an architecture
-- Mostly used in simulation environments than synthesis
configuration config of hello is
    for arch
        --
    end for;
    -- can be associated with multiple architectures
end configuration ; -- config

-- Package: Collection of reusable code that can be referenced by VHDL designs
-- Consists of a package declaration and a package body (for functions)
-- two built-in packages: standard (types/operators) and TEXTIO (file io)

-- Libraries: contain a packege or a collection of them (a directory)
-- Standard package, IEEE package, made by silicon vendors or user-made
-- Working library is the lib which the unit is being compiled into

-- All packages must be compiled, otherwise they end up in implicit libraries:
-- WORK or STD - these do not need to be explicitly called out to be used
-- To use an explicit package from another library (usually atop the file)
library IEEE;
use IEEE.std_logic_1164.all;

-- The standard package defines default types: bit, boolean, integer, real, time
-- textio defines file operations. Since it's built-in, no need to call it

-- Standard package types:
-- bit - logic value 1/0, append _vector to indicate array of bits.
--       bit_vector (3 downto 0); or (0 to 3) indicating MSB position
--       but downto is most common.
-- boolean - true/false
-- integer - pos and neg values in decimal (32 bit max?)
--           to limit the value, we can say x : integer range 0 to 255 (8 bit
--           int)
-- character - ascii char
-- string - array of characters
-- time - value includes units of time (ps, us, ns ,ms, sec, min, hr)

-- IEEE Library contains std types, arithmetic signed and unsigned functions
-- IEEE_logic_1164 package types:
-- std_logic - 9 logic value system
--             1 - logic high       H - weak logic high
--             0 - logic low        L - weak logic low
--             X - unknown          W - weak unknown
--             U - undefined        Z - tri-state
--             - - don't care
-- the standard logic allows for multiple signal drivers, for example two drivers
-- can drive the bus with 1 and Z, the std_logic will resolve this to 1. If the
-- same happened with values 1 and 0, the result would be X.
-- std_ulogic - the same as std_logic, but does not supporte multiple signal
--              drives, resulting in an error

---------------------
-- VHDL Constructs --
---------------------
--
-- Constants: Same as other lang constants, with local (arch) and global (entity)
constant bus_width : integer := 16;

-- Signals: Represent phy interconnect (wire) that communicate between processes
--          (functions). Can be declared in Packages, Entity and Architecture
-- Signals in Entity are essentially I/O
-- Signals in Architecture are internal signals to connect Entities together,
--         invisible to the outside world.
-- Note: vars are signals by default in the entities ports declaration
signal temp : std_logic_vector (7 downto 0);

-- To assign values to signals, '1' for single bit and "101" multi bit assign
temp <= "10101010";          -- bus (vector) value
temp <= x"AA";               -- 93 standard also supports HEX
temp(7) <= '1';              -- Assign a single array bit
temp (7 downto 4) <= "1010"; -- Bit-slicing, like python temp[4:7]

-- Signal assignment <= is an implied process (function) that will synthesize
-- to hardware. Any time the input to the process changes on the right, that
-- process (function) is executed and assigns the result value to the signal.
-- signal <= process

-- Operators in VHDL:
-- Logical: NOT, AND, OR, NAND, NOR, XOR, XNOR (93 only)
-- Relational: =, /= (not equal), <, <=, >, >=
-- Arithmetic: +, -, *, /, mod, rem
-- Misc: ** (exponent), abs, & (concat, say 2 vectors of 4bit, into one 8bit)

--------------------------
-- Operator Overloading --
--------------------------
--
-- Same concept as in other languages, re-define operator behaviour for user
-- data types. Operators are overloaded by defining a function whose name is
-- the same as the operator itself. Normally declared in a package so that it
-- is visible for any design. Enclose the operator in "" when defining functions.
function "+" (l: std_logic_vector; r: integer) return std_logic_vector;
-- To use the overloaded operator, one must use the package where it's defined:
library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.std_logic_unsigned.ALL;

signal a: in std_logic_vector (4 downto 0);
signal b: in std_logic_vector (4 downto 0);
signal sum: out std_logic_vector (4 downto 0);
sum <= a + b; -- overloaded + on non-built-in data type

-----------------------------------
-- Concurrent Signal Assignments --
-----------------------------------
--
-- (in Architecture body)
-- Take expressions and assign the result to a signal. Represent implied processes
-- that execute in parallel. Trigger from anything on read (right) side of the
-- assignment, executes the process and assignes a new value to the signal on
-- the left. Multiple assignments are concurrent and run in parallel.

-- Simple signal assignment:
-- signal_name <= expression; Implied concurrent processes, order doesn't matter
qa <= r or t;
qb <= (qa and not(g xor h));

-- Conditional signal assignment:
-- Assigned upon meeting a pre-defined condition, has to end with an assignable
-- expression like '0' to deal with default (other) cases

-- Priority MUX
sig_xor <= '1' when a='0' and b='1' else
           '1' when a='1' and b='0' else
           '0';
sig_and <= '1' when a='1' and b='1' else
           '0';
q <= a when sela = '1' else
     b when selb = '1' else
     c;

-- Selected signal assignment:
-- Similar to case statements, selecting upon meeting a condition. sel is the
-- signal we're "casing" on, and assigning to q when values are "00", "01" etc.
-- this is also an implied process, just like other signal assignments in the
-- architecture body.

-- Wide MUX: All values have an equal chance of being true, no priority.
with sel select
    q <= a when "00",
         b when "01",
         c when "10",
         d when others;
-- We can't simply replace "others" with "11" because in std_logic there are
-- other possible values like 'X', 'Z', 'U' etc.

-----------------------------
-- Signal Assignment Delay --
-----------------------------
--
-- Inertial delay: (default)
-- A pulse that is short in duration of the specified delay (10 ns) will not be
-- transmitted in this case.
a <= b after 10 ns; -- the statement waits 10ns before a is assigned to b

-- Transport delay
-- Any pulse is transmitted, no matter how short it was, and assigned to signal
a <= transport b after 10 ns;

-- These pulse durations are set by the simulation tool and used only there, so
-- these need to be set in the simulation tool beforehand.

--------------------------------
-- Explicit Process Statement --
--------------------------------
--
-- Unlike implicit signal processes, we can create explicit process that is run
-- infinitely unless broken by a WAIT statement or sensitivity list. Inside of
-- an explicit process, the statements are executed sequentially. The process
-- itself is still executed concurrently with others, but individual statements
-- inside an explicit process, are sequential.
-- label: process (sensitivity_list)
--    constants
--    types
--    variable
-- begin
--    sequential statements
-- end process;

-- The process is sensitive to a or b, if they change, the process is turned on
-- and executes the statements inside. Once it's done, it'll wait for another
-- transition on a or b
proc1: process (a, b)
begin
    -- sequential statements
end process;

-- With no sensitivity list, this starts execution right from the start of the
-- simulation. The wait on statement forces the process to wait until a or b
-- transition (change/pulse etc.), before it loops around and starts executing
-- again.
proc2: process
    -- sequential statements
    wait on (a, b);
end process;

-- Sequential Statements: Must be used inside of an explicit process. Indicate
-- behavior and express order

-- signal assignment (same as others)
a <= b;

-- if-then
-- conditional signal assignments can not be used in explicit processes, we can
-- only use a series of priority MUXs. Conditional assignments are implied proc
-- on their own and can not be used as a sequential statement.
sel: process (sela, selb, a, b, c)
begin
    if sela = '1' then
        q <= a;
    elsif selb = '1' then
        q <= b;
    else
        q <= c;
    end if;
end process;

-- case
-- selected signal assignments can not be used in explicit processes, as it is
-- an implied process on its own. Same as if-then statement
-- 4 input MUX
process (sel, a, b, c, d)
begin
    case sel is
        when "00" =>
            q <= a;
        when "01" =>
            q <= b;
        when "10" =>
            q <= c;
        when others =>
            q <= d;
    end case;
end process;

-- looping (all loops support next and exit)
-- loop: infinitely unless exit statement exists
loop_label loop
    -- sequential statements
    next loop_label when ; -- stop with current execution, and re-run the loop
    exit loop_label when ; -- exit upon condition
end loop;

-- while: conditional test to end loop
while condition loop
    -- sequential statements
end loop;

-- for (iteration)
for identifier in range loop
    -- sequential statements
end loop;

-- wait (limited synthesis usage)
-- used to pause execution of a process until some condition is satisfied
wait on a, b; -- wait on <signal>
wait until (int < 100); -- pause unti lboolean expression is true
wait for 20 ns; -- pause until time expression elapses
wait until (a = '1') for 5 us; -- can be combined, if a isn't '1' in 5 us

--------------------------------
-- Delta and Simulatin Cycles --
--------------------------------
--
library IEEE;
use IEEE.std_logic_1164.ALL;

entity simp is
  port (
    a, b : in std_logic;
    y : out std_logic
  ) ;
end entity ; -- simp

architecture logic of simp is
    signal c : std_logic;
begin
    ---------------------------
    -- Equivalent Functions ---
    ---------------------------
    -- Implied processes:
    -- this will take 2 delta cycles, but 1 simulation cycle to complete
    c <= a AND b;
    y <= c;

    -- Explicit processes:
    -- this will also take 2 delta cycles, and 1 simulation cycle
    proc_eq: process (a, b)
    begin
        c <= a AND b;
    end process;

    proc2: process(c)
        y <= c;
    end process;

    -----------------------------
    -- Unequivalent Functions ---
    -----------------------------
    -- implied
    c <= a AND b;
    y <= c;

    -- explicit, but with the same signal assignment
    -- this will take 2 simulation cycles to be executed
    proc_neq: process(a, b)
    begin
        -- the C is scheduled to be updated, but doesn't propagate until the
        -- next delta cycle
        c <= a AND b;
        -- the Y will get the old value of C and will only update once the
        -- process ends, so in order for Y to see the new value of C it would
        -- take a second iteration of the process (a or b has to change).
        y <= c;
    end process;

    --------------------------------
    -- Equivalent with Variables ---
    --------------------------------
    -- to work around the above unequivalency, VHDL uses variables
    -- this will take 1 simulation cycles to be executed, like the orig function
    -- see variable description below
    proc_eq_with_vars: process(a, b)
        -- C is made a variable in local scope
        variable c: std_logic;
    begin
        -- if A or B trigger the process, the value of C is updated immediately
        -- with no delay to wait for the process to finish
        c := a AND b;
        -- Y will take the new value of C within the same simulation cycle
        y <= c;
    end process;
end architecture ; -- logic

---------------------------
-- Variable Declarations --
---------------------------
--
-- variables are declared inside a process
-- assignment is represented by :=

-- assignments are updated immediately and do not incur a delay, ie do not wait
-- for the process to end to propagate the result value

-- declare:
-- variable <name> : <data_type> := <value>;
variable temp : std_logic_vector (7 downto 0);
-- operations are the same as signal assignment
temp := "10101010";         -- all bits
temp := x"AA";              -- in VHDL 93
temp(7) := '1';             -- single bit
temp(7 downto 4) := "1010"; -- bit slicing

---------------------------
-- Signals vs. Variables --
---------------------------
--            |          Signals          |        Variables
--------------------------------------------------------------------
-- Assignment |     y <= c                | y := c                 |
-- Utility    |     Circuit interconnect  | Local storage          |
-- Scope      |     Global (between proc) | Local (inside process) |
-- Behavior   |     End of delta update   | Immediate update       |
--------------------------------------------------------------------

-----------------------------
-- User Defined data types --
-----------------------------
--
-- arrays and enums
architecture logic of my_memory is

    -- array is a 2d *datatype* for storing values, must create constant signal
    -- or variable of that type, used to create memories and store simulation
    -- vectors type <array_type_name> is array (<integer_range) of <data_type>;

    -- create new array datatype "mem" which has 64 addr locations,
    -- each 8 bit wide
    type mem is array(0 to 63) of std_logic_vector (7 downto 0);

    -- create 2 - 64x8-bit arrays (of user defined type mem) to use in design
    -- these are 512 bit/64 bytes vectors
    signal mem_64x8_a, mem_64x8_b : mem;

    -- enum is a *datatype* with name and values, used to make code readable
    -- and in finate state machines, just like C typedef STATE enum {A=1, B=2};
    -- type <enum_type_name> is (enum, states, or, values);
    type enum is (idle, fill, heat, wash, drain);

    -- create a state signal
    signal dishwasher_state : enum;

begin
    -- use our defined type signals to store data
    -- access individual array vectors with (xx)
    mem_64x8_a(12) <= x"F0";
    mem_64x8_b(50) <= "11110000";

    -- poll current state, using defined enumerator
    drain_led <= '1' when dishwasher_state = drain else '0';
end architecture; -- logic

---------------------
-- Logic Synthesis --
---------------------
--
-- RTL Synthesis refers to the act of reading HDL code, and translating it
-- into hardware. Synthesis tool reads the code, translates it into gates, runs
-- optimization to make it smaller and faster. Two types of processes are used:

-- Combinatorial Process: Sensitive to *all* inputs used in combinatorial logic
-- Any signal used on the right hand side (the assignment) inside your process
-- needs to be in the sensitivity list
comb: process(a, b, sel)
    variable c: std_logic;
begin
    c := a AND b;
    q <= c OR sel;
end process;

-- While RTL Synth will assume that you meant to include all inputs in your
-- sensitivity list and will fill in the missing signals. The simulation tool
-- will execute exactly as written. Thus, Sim and Synth processes can differ.

-- Sequential Process: Sensitive to a clock or/and any async control signals,
-- for example RESET, CLEAR etc. Used for building registers.

-- example D Flip-Flop with async clear signal
library IEEE;
use IEEE.std_logic_1164.ALL;

entity dff_b is
  port (
    clr, clk, d, ena: in std_logic;
    q: out std_logic
  );
end entity;

architecture rtl of dff_b is
begin
    -- if the clock transitions, this process triggers
    process(clk, clr)
    begin
        -- check the clear signal first (async control signal), clear signal
        -- does not depend on the clock and checked before the clock check
        if (clr = '0') then
            q <= '0';

        -- IEEE function, signal must be 0 to 1
        -- any other clk transition will fail (1 to 0, X, Z etc)
        elsif (rising_edge(clk)) then
            q <= d;
        end if;

        -- to have clear synched with the clk, we check the clear inside the
        -- rising_edge(), clr='0' now depends on the clock, clk shouldn't be
        -- present in the sensitivity list
        if (rising_edge(clk)) then
            if (clr = '0') then
                q <= '0';
            else
                q <= d;
        end if;

        -- to combine both of these together: DFF Async clear & Clock Enable
        if (clr = '0') then
            q <= '0';
        elsif (rising_edge(clk)) then
            if (ena = '1') then
                q <= d;
            end if;
            -- if ena /= 1, the register retains previous value
        end if;

    end process;
end architecture;

-- Always stick to either combinatorial or sequential process definition.

-- rising_edge() and falling_edge() functions are recommended to use with clk
-- detection, compared to the older method of (clk'event and clk='1')

-- Synthesizing Registers:
-- Signal assignments infer registers when placed inside if-then statements
-- that check for clock condition.
process (clk)
begin
    -- The following will generate a register (d flip-flop)
    if (rising_edge(clk)) then
        -- each statement will generate individual register
        q <= d;
    end if;
end process;

-- Counter:
-- Are accumulators (register + adder) that always add/substract a '1'
library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.std_logic_unsigned.ALL;

entity counter is
  port (
    clk, rst: in std_logic;
    q: out std_logic_vector(15 downto 0)
  ) ;
end entity ;

architecture logic of counter is
    signal tmp_q: std_logic_vector(15 downto 0);

begin
    process (clk, rst)
    begin
        if (rst = '0') then
            -- just another way of setting all vector elements to one value
            tmp_q <= (others >= '0');
        elsif (rising_edge(clk)) then
            -- count temp q on each rising clock
            tmp_q <= tmp_q + 1;
        end if;
    end process;
    -- assign the value output of the counter entity
    q <= tmp_q;

end architecture ; -- logic

-------------------------
-- Designing Hierarchy --
-------------------------
--
-- Design Hierarchically - Multiple Design Files
-- VHDL hierarchical design requires Component Declarations and Component
-- Instantiations.
--
-- This is the same concept as having multiple interfaces hidden from the top
-- layer component, for example via header files. The top layer needs not to
-- know the very bottom layer individual components in order to operate.
-- top <- mid <- bottom: the top layer, has no idea about the bottom layer.

-- Component Declaration & Instantiation
--
library IEEE
use IEEE.std_logic_1164.ALL;
entity tolleab is
  port (
    tclk, tcross, tnickel, tdime, tquarter: in std_logic;
    tgreen, tred: out std_logic
  ) ;
end entity ; -- tolleab

architecture tolleab_arch of tolleab is

-- Declaration: Used to declare the port-types and the data types of the ports
-- for a lower-level design.
    component tollv
        port (
            clk, cross, nickel, dime, quarter: in std_logic;
            green, red: out std_logic
        );
    end component;
begin

-- Instantiation: Concurrent statement used to map the ports of a lower-level
-- design to that of the upper-level design.

-- We're mapping lower-level inputs of tollv to upper-level tolleab

U1: tollv port map (
        clk => tclk,
        cross => tcross,
        nickel => tnickel,
        dime => tdime,
        quarter => tquarter,
        green => tgree,
        red => tred
    );

-- U1 is the name of the instance, we can create multiple instances with
-- various mappings of the same block tolleab.

end architecture ; -- tolleab_arch

-------------
-- Summary --
-------------
--
-- VHDL Design Units and Constructs to create Models:
-- Entity, Architecture, Signals, Variables, Processes, Logic Synth, Hirarchy
